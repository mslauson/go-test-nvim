local operations = require("gotests.operations")

--@class gotests
local gotests = {
	functions_menu = operations.buffer_functions_menu,
}

return gotests
