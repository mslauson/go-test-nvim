local M = {}

local Menu = require("nui.menu")
local lsp_utils = require("gotests.utils.lsp")
local ui = require("gotests.ui")

M.buffer_functions_menu = function()
	local funcs = lsp_utils.get_all_functions_in_buffer()

	local menuOpts = {
		title = "Buffer Functions 󰊕",
		items = {},
	}

	for _, func in ipairs(funcs) do
		table.insert(
			menuOpts.items,
			Menu.Item({
				text = func.name,
				highlight = "Normal",
				callback = function()
					vim.notify("Function: " .. func.name)
				end,
			})
		)
	end

	ui.open_menu(menuOpts)
end

return M
