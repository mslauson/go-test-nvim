local M = {}

-- Function to get all function names in the buffer
M.get_all_functions_in_buffer = function()
	local all_functions = {}
	local function on_document_symbols(_, _, result)
		if not result then
			return
		end

		local function collect_functions(symbols)
			for _, symbol in ipairs(symbols) do
				if symbol.kind == 12 then -- SymbolKind.Function
					table.insert(all_functions, symbol.name)
				end
				if symbol.children then
					collect_functions(symbol.children)
				end
			end
		end

		collect_functions(result)
		print("All functions in buffer: " .. vim.inspect(all_functions))
	end

	vim.lsp.buf_request(0, "textDocument/documentSymbol", vim.lsp.util.make_text_document_params(), on_document_symbols)

	return all_functions
end

-- Function to get the closest function to the cursor
M.get_closest_function_to_cursor = function()
	local closest_function = nil
	local function on_document_symbols(_, _, result)
		if not result then
			return
		end

		local row, col = unpack(vim.api.nvim_win_get_cursor(0))
		row = row - 1 -- Adjust because Lua is 1-indexed, LSP is 0-indexed

		local function find_closest_function(symbols)
			for _, symbol in ipairs(symbols) do
				if
					symbol.kind == 12 -- SymbolKind.Function
					and (row > symbol.range.start.line or (row == symbol.range.start.line and col >= symbol.range.start.character))
					and (
						row < symbol.range["end"].line
						or (row == symbol.range["end"].line and col <= symbol.range["end"].character)
					)
				then
					closest_function = symbol.name
				end
				if symbol.children then
					find_closest_function(symbol.children)
				end
			end
		end

		find_closest_function(result)
		if closest_function then
			print("Closest function to cursor: " .. closest_function)
		else
			print("No function close to cursor.")
		end
	end

	vim.lsp.buf_request(0, "textDocument/documentSymbol", vim.lsp.util.make_text_document_params(), on_document_symbols)

  return closest_function
end

return M
